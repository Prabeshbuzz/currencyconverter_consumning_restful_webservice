package com.prabeshbuzz.currencyconverter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.prabeshbuzz.currencyconverter.controller.model.MappingFields;
import com.prabeshbuzz.currencyconverter.restClient.RestTemplateModule;

@Controller
@RequestMapping("/currencyconverter")
public class CurrencyController {
	@Autowired
	private RestTemplateModule restTemplatemodule;
	MappingFields getResource = RestTemplateModule.restTemplate();
	
	@GetMapping("/convert")
	public String ConvertCurrency(Model theModel) {
		theModel.addAttribute("base", getResource.getBase());
		theModel.addAttribute("date", getResource.getDate());
		theModel.addAttribute("rates", getResource.getRates());
		return "CurrencyConverter";
	}
}
