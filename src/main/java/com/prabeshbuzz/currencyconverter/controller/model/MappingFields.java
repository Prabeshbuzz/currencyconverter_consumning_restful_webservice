package com.prabeshbuzz.currencyconverter.controller.model;

import java.util.Date;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MappingFields {
	@JsonProperty("sucess")
	private boolean success;
	@JsonProperty("timestamp")
	private long timestamp;
	@JsonProperty("base")
	private String base;
	@JsonProperty("date")
	private Date date;
	@JsonProperty("rates")
	private Map<String, Float> rates;

	public boolean isSuccess() {
		return success;
	}

	public MappingFields() {

	}

	public MappingFields(boolean success, long timestamp, String base, Date date, Map<String, Float> rates) {
		super();
		this.success = success;
		this.timestamp = timestamp;
		this.base = base;
		this.date = date;
		this.rates = rates;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Map<String, Float> getRates() {
		return rates;
	}

	public void setRates(Map<String, Float> rates) {
		this.rates = rates;
	}

	@Override
	public String toString() {
		return "MappingFields [success=" + success + ", timestamp=" + timestamp + ", base=" + base + ", date=" + date
				+ ", rates=" + rates + "]";
	}

}
