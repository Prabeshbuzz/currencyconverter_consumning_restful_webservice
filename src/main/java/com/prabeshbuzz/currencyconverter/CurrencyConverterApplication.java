package com.prabeshbuzz.currencyconverter;

import java.util.Collections;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


import com.prabeshbuzz.currencyconverter.controller.model.MappingFields;

@SpringBootApplication
public class CurrencyConverterApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CurrencyConverterApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		String resourcesURL = "http://www.cerotid.com/services/api/currencyconverter/getallcurrencyrate";
		HttpEntity<String> entity = new HttpEntity<String>(headers);

		ResponseEntity<MappingFields> response = restTemplate.exchange(resourcesURL, HttpMethod.GET, entity, MappingFields.class);
		System.out.println("The result");
		System.out.println(response.getBody().getRates());
		MappingFields cs = response.getBody();
		System.out.println(cs.getBase());
		System.out.println(cs.getRates().get("USD"));
		System.out.println(cs.getRates().keySet());
		
		

	}

}
