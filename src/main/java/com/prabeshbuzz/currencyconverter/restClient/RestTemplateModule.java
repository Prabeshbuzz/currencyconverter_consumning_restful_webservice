package com.prabeshbuzz.currencyconverter.restClient;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.prabeshbuzz.currencyconverter.controller.model.MappingFields;

@Configuration
public class RestTemplateModule {
	@Bean
	public static MappingFields restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		String resourcesURL = "http://www.cerotid.com/services/api/currencyconverter/getallcurrencyrate";
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<MappingFields> response = restTemplate.exchange(resourcesURL, HttpMethod.GET, entity,
				MappingFields.class);
		//check if the reult is coming back 
		System.out.println("The result");
		System.out.println(response.getBody().getRates());
		
		MappingFields finalresponse = response.getBody();
		return finalresponse;

	}
}
