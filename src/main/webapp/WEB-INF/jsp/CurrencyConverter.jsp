<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<link href="../../webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet" />
<script src="../../webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="../../webjars/jquery/3.0.0/js/jquery.min.js"></script>
<link href="/css/main.css" rel="stylesheet">
<meta charset="ISO-8859-1">
<title>CurrencyConverter</title>
</head>
<body>
	<!--    navigation bar  -->

	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<a class="navbar-brand" href="#">PrabeshBuzz</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link" href="#">Home
						<span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link" href="#">About</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Contact
						us</a></li>
			</ul>
		</div>
	</nav>
	<div class="container">
		<h3> Currency Converter</h3>
	
	<h3> The base is ${base} </h3>
	</div>
     <div id="rates"><h3>The Exchange rate is :</h3> ${rates}
     <br>
     <h3>Select Countries :</h3>
     <select name="rates">
    <c:forEach items="${rates}" var="hello">
    		
          <option >${hello.key}</option>
    </c:forEach>
</select>


 </div>
</body>
</html>